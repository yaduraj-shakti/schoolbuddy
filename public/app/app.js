(function () {

    var app = angular.module('app', ['ngRoute']);

    app.config(['$logProvider', '$routeProvider', function ($logProvider, $routeProvider) {

        $logProvider.debugEnabled(true);
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                controllerAs: 'home',
                // template:'<h1>This is an inline template</h1>'
                templateUrl: '/app/templates/home.html'
            })
            .when('/schools', {
                controller: 'AllSchoolsController',
                controllerAs: 'schools',
                templateUrl: '/app/templates/allSchools.html'
            })
            .when('/classrooms', {
                controller: 'AllClassroomsController',
                controllerAs: 'classrooms',
                templateUrl: '/app/templates/allClassrooms.html'
            })
            /* Below code is demo for redirectTo feature*/
            // .when('/classrooms/:id', {
            //     controller: 'AllClassroomsController',
            //     controllerAs: 'classrooms',
            //     templateUrl: '/app/templates/allClassrooms.html',
            //     // redirectTo:'/schools'
            //     redirectTo: function (params, currPath, currSearch) {
            //         console.log(params);
            //         console.log(currPath);
            //         console.log(currSearch);
            //         return '/';
            //     }
            // })
            .when('/classrooms/:id', {
                controller: 'ClassroomController',
                controllerAs: 'classroom',
                templateUrl: '/app/templates/classroom.html'
            })
            .when('/classrooms/:id/detail/:month?', {
                controller: 'ClassroomController',
                controllerAs: 'classroom',
                templateUrl: '/app/templates/classroomDetail.html'
            })
            .when('/activities', {
                controller: 'AllActivitiesController',
                controllerAs: 'activities',
                templateUrl: '/app/templates/allActivities.html',
                resolve: {
                    //activities is a controller. the result will be injected to this controller
                    activities: function (dataService) {
                        return dataService.getAllActivities();
                    }
                }
            })
            .otherwise('/');


    }]);

    //keep any initialization code withing run method
    app.run(['$rootScope', '$log', function ($rootScope, $log) {
        $rootScope.$on('$routeChangeSuccess', function (event, current, previouse) {
            $log.debug('Successfully changed routes');
            $log.debug(event);
            $log.debug(current);
            $log.debug(previouse);
        });

        $rootScope.$on('$routeChangeError', function (event, current, previouse, rejection) {
            $log.debug('Error changing routes');
            $log.debug(event);
            $log.debug(current);
            $log.debug(previouse);
            $log.debug(rejection);
        });

    }]);

}());